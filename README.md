This program runs on number of people(as a input and provide [sort list](https://en.wikipedia.org/wiki/Sorting_algorithm) of names). It is implemented in [Ruby](https://en.wikipedia.org/wiki/Ruby_%28programming_language%29).

Input: Ramos, Jonas, Abell, Zenith, Yumrag
Ouptut: Abell, Bella, Jonas, Ramos, Yumrag, Zenith

It will [sort](https://en.wikipedia.org/wiki/Structure_%28mathematical_logic%29#Many-sorted_structures) out any input of names and output will properly sorted. Look [here](https://www.chicagoexcelclasses.com/) to get more information. Apeiros is the author of the program and he reserve the write of distribution.